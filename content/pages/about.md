+++
title = "About"
date = 2018-03-01
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.9 + ox-hugo)"
weight = 2001
noauthor = true
nocomment = true
nodate = true
nopaging = true
noread = true
[menu.main]
  weight = 2001
  identifier = "about"
+++

This is the newest version of my "Numbers and Shapes" blog, containing material
on elementary mathematics, mathematics education, software, and anything else
that takes my fancy.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
